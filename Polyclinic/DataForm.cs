﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using ClassLibraryPolyclinic;
using System.IO;

namespace Polyclinic
{
    public partial class DataForm : Form
    {
        public DataForm()
        {
            InitializeComponent();
        }

        private void labelClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBoxName_Click(object sender, EventArgs e)
        {
            if (textBoxName.Text == "name")
                textBoxName.Text = "";
            labelError.Visible=false;
        }
        private void textBoxSurname_Click(object sender, EventArgs e)
        {
            if (textBoxSurname.Text == "surname")
                textBoxSurname.Text = "";
            labelError.Visible = false;
        }
        private void textBoxPatronymic_Click(object sender, EventArgs e)
        {
            if (textBoxPatronymic.Text == "patronymic")
                textBoxPatronymic.Text = "";
            labelError.Visible = false;
        }
        private void textBoxBirthday_Click(object sender, EventArgs e)
        {
            if (textBoxBirthday.Text == "birthday")
                textBoxBirthday.Text = "";
            labelError.Visible = false;
        }
        private void textBoxTel_Click(object sender, EventArgs e)
        {
            if (textBoxTel.Text == "number tel")
                textBoxTel.Text = "";
            labelError.Visible = false;
        }
        private void textBoxPassport_Click(object sender, EventArgs e)
        {
            if (textBoxPassport.Text == "number pass")
                textBoxPassport.Text = "";
            labelError.Visible = false;
        }
        private void textBoxDoctor_Click(object sender, EventArgs e)
        {
            if (textBoxDoctor.Text == "doctor")
                textBoxDoctor.Text = "";
            labelError.Visible = false;
        }

        Regex regex;
        private void textBoxName_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^[А-ЯЇЄІ][а-яїєі']{2,12}$", RegexOptions.Compiled);
            
            if (regex.IsMatch(textBoxName.Text))
                textBoxName.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxName.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxSurname_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^[А-ЯЇЄІ][а-яїєі']{2,10}$", RegexOptions.Compiled);
            
            if (regex.IsMatch(textBoxSurname.Text))
                textBoxSurname.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxSurname.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxPatronymic_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^[А-ЯЇЄІ][а-яїєі']{6,12}$", RegexOptions.Compiled);
            
            if (regex.IsMatch(textBoxPatronymic.Text))
                textBoxPatronymic.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxPatronymic.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxBirthday_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^((0[1-9]|[1-2][\d]|3[0-1])\.(0[1-9]|1[0-2])\.(192[2-9]|19[3-9][\d]|20[0-1][\d]|202[0-1])|(0[1-9]|1[\d]|2[0-1])\.(0[1-7])\.2022)$", RegexOptions.Compiled);
            
            if (regex.IsMatch(textBoxBirthday.Text))
                textBoxBirthday.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxBirthday.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxTel_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^(\+?38)?0(39|50|6[36-8]|73|89|9[1-9])(\d{3}\d{2}\d{2})$", RegexOptions.Compiled);
            
            if (regex.IsMatch(textBoxTel.Text))
                textBoxTel.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxTel.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxPassport_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^((192[2-9]|19[3-9][\d]|20[0-1][\d]|202[0-1])(0[1-9]|[1-2][\d]|3[0-1])(0[1-9]|1[0-2])(-\d{5})|2022(0[1-9]|1[\d]|2[0-1])(0[1-7])(-\d{5}))$", RegexOptions.Compiled);
            
            if (regex.IsMatch(textBoxPassport.Text))
                textBoxPassport.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxPassport.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxDoctor_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^([А-ЯЇЄІ][а-яїєі']{2,15}\s[А-ЯЇЄІ][а-яїєі']{2,15}\s[А-ЯЇЄІ][а-яїєі']{5,15})$", RegexOptions.Compiled);
            
            if (regex.IsMatch(textBoxDoctor.Text))
                textBoxDoctor.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxDoctor.ForeColor = Color.FromArgb(255, 106, 106);
        }

        
        private void buttonAddPatient_Click(object sender, EventArgs e)
        {
            if(textBoxName.ForeColor == Color.FromArgb(147, 255, 174) && textBoxSurname.ForeColor== Color.FromArgb(147, 255, 174)&& textBoxPatronymic.ForeColor== Color.FromArgb(147, 255, 174)&& textBoxBirthday.ForeColor== Color.FromArgb(147, 255, 174)&& textBoxTel.ForeColor== Color.FromArgb(147, 255, 174)&& textBoxPassport.ForeColor== Color.FromArgb(147, 255, 174)&&textBoxDoctor.ForeColor== Color.FromArgb(147, 255, 174))
            {
                Patient patient = new Patient(textBoxSurname.Text, textBoxName.Text, textBoxPatronymic.Text, textBoxBirthday.Text, textBoxTel.Text, textBoxPassport.Text, textBoxDoctor.Text);

                labelError.Text = "";

                
                string[] allPatients = File.ReadAllLines(PathToFiles.patients);
                string[] allDoctors = File.ReadAllLines(PathToFiles.doctors);
                

                if (allPatients.Length > 0 && allDoctors.Length>1)
                {
                    Regex regexNameCheck = new Regex($@"{patient.Name}", RegexOptions.Compiled);
                    Regex regexSurnameCheck = new Regex($@"{patient.Surname}", RegexOptions.Compiled);
                    Regex regexPatronymicCheck = new Regex($@"{patient.Patronymic}", RegexOptions.Compiled);

                    for (int i = 0; i < allPatients.Length; i++)
                    {
                        if(regexNameCheck.IsMatch(allPatients[i])&&regexSurnameCheck.IsMatch(allPatients[i]) && regexPatronymicCheck.IsMatch(allPatients[i]))
                        {
                            labelError.Text = "Цей пацієнт вже доданий";
                            labelError.Visible = true;
                        }
                    }
                    if (string.IsNullOrWhiteSpace(allPatients[0]))
                        File.WriteAllText(PathToFiles.patients, string.Empty);
                }
                if (allPatients.Length == 0 || string.IsNullOrWhiteSpace(allPatients[0]))
                {
                    string line = $"Прізвище:\tІм'я:\tПо-батькові:\tДатаНародж:\tТелефон:\tПаспорт:\tЛікар:";

                    using (StreamWriter file = new StreamWriter(PathToFiles.patients, true, Encoding.UTF8))
                        file.WriteLine(line);
                }

                bool flag = false;
                regex = new Regex($@"{patient.Doctor}", RegexOptions.Compiled);
                if (allDoctors.Length > 1)
                {
                    for(int i = 1; i < allDoctors.Length; i++)
                    {
                        
                        string[] line = allDoctors[i].Split();
                        for (int j = 0;  j< line.Length; j++)
                            line[j] = line[j].Trim();
                        if(regex.IsMatch($"{line[0]} {line[1]} {line[2]}"))
                        {
                            int count = 0;
                            int maxCount = int.Parse(line[5]);
                            if (allPatients.Length > 1)
                            {
                                for(int l = 1; l < allPatients.Length; l++)
                                {
                                    if(regex.IsMatch(allPatients[i]))
                                        count++;
                                }
                            }
                            if (count < maxCount)
                                flag = true;
                        }

                    }
                }
                

                if (labelError.Text!= "Цей пацієнт вже доданий" && flag)
                {
                    string line = $"{patient.Surname}\t{patient.Name}\t{patient.Patronymic}\t{patient.Birthday}\t{patient.Tel}\t{patient.Passport}\t{patient.Doctor}";

                    using (StreamWriter file = new StreamWriter(PathToFiles.patients, true, Encoding.UTF8))
                        file.WriteLine(line);

                    textBoxName.Text = textBoxSurname.Text = textBoxPatronymic.Text = textBoxBirthday.Text = textBoxTel.Text = textBoxPassport.Text = textBoxDoctor.Text = "";
                    dataGridViewPatients.Visible = false;
                }
                else
                {
                    labelError.Text = "Перевірте інформацію\nпро лікаря";
                    labelError.Visible = true;
                }

            }
            else
            {
                labelError.Text = "Помилка у введені";
                labelError.Visible = true;
            }
        }
        private void buttonShowPatients_Click(object sender, EventArgs e)
        {
            dataGridViewPatients.AllowUserToAddRows = false;
            dataGridViewPatients.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewPatients.AllowUserToResizeColumns = false;
            dataGridViewPatients.AllowUserToResizeRows = false;
            dataGridViewPatients.Visible = true;
            labelError.Visible = false;

            string[] allPatients = File.ReadAllLines(PathToFiles.patients);

            if(allPatients.Length > 0)
            {
                DataTable table = new DataTable();

                string[] line = allPatients[0].Split();
                for (int i = 0; i < line.Length; i++)
                {
                    line[i] = line[i].Trim();
                    table.Columns.Add($"{line[i]}", typeof(string));
                }

                for (int i = 1; i < allPatients.Length; i++)
                {
                    line=allPatients[i].Split();
                    for(int j = 0; j < line.Length; j++)
                    {
                        line[j] = line[j].Trim();
                    }
                    Patient patient = new Patient(line[0],line[1],line[2],line[3],line[4],line[5],$"{line[6]} {line[7]} {line[8]}");
                    table.Rows.Add(patient.Surname,patient.Name,patient.Patronymic,patient.Birthday,patient.Tel,patient.Passport,patient.Doctor);
                }
                dataGridViewPatients.DataSource = table;
                dataGridViewPatients.Columns[6].Width = 190;

                for (int i = 0; i < 7;i++)
                {
                    dataGridViewPatients.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }
            if (allPatients.Length == 1 || allPatients.Length < 1)
            {
                dataGridViewPatients.Visible = false;
                labelError.Text = "Пацієнти відсутні";
                labelError.Visible = true;
            }          
        }
        private void buttonRemovePatient_Click(object sender, EventArgs e)
        {
            string[] allPatients = File.ReadAllLines(PathToFiles.patients);

            if (dataGridViewPatients.Columns.Count == 7)
            {
                if (allPatients.Length > 1)
                {
                    allPatients[dataGridViewPatients.CurrentCell.RowIndex + 1] = "";
                    File.WriteAllText(@"C:\Users\Acer\OneDrive\Робочий стіл\data.txt", string.Empty);

                    using (StreamWriter writer = new StreamWriter(PathToFiles.patients))
                    {
                        foreach (string str in allPatients)
                        {
                            if (!string.IsNullOrWhiteSpace(str))
                                writer.WriteLine(str);
                        }
                    }

                    buttonShowPatients_Click(sender, e);
                }
                else
                {
                    labelError.Text = "Пацієнти відсутні";
                    labelError.Visible = true;
                }
            }
            else if (dataGridViewPatients.Columns.Count > 0 && dataGridViewPatients.Columns.Count < 7)
            {
                labelError.Text = "Пацієнти відсутні";
                labelError.Visible = true;
            }
            else
            {
                labelError.Text = "Ви працюєте не\nз пацієнтами";
                labelError.Visible = true;
            }
        }



        private void textBoxSurnameDoc_Click(object sender, EventArgs e)
        {
            if (textBoxSurnameDoc.Text == "surname")
                textBoxSurnameDoc.Text = "";
            labelErrorDoc.Visible = false;
        }
        private void textBoxNameDoc_Click(object sender, EventArgs e)
        {
            if (textBoxNameDoc.Text == "name")
                textBoxNameDoc.Text = "";
            labelErrorDoc.Visible = false;
        }
        private void textBoxPatronDoc_Click(object sender, EventArgs e)
        {
            if (textBoxPatronDoc.Text == "patronymic")
                textBoxPatronDoc.Text = "";
            labelErrorDoc.Visible = false;
        }
        private void textBoxTelDoc_Click(object sender, EventArgs e)
        {
            if (textBoxTelDoc.Text == "number tel")
                textBoxTelDoc.Text = "";
            labelErrorDoc.Visible = false;
        }
        private void textBoxPositionDoc_Click(object sender, EventArgs e)
        {
            if (textBoxPositionDoc.Text == "position")
                textBoxPositionDoc.Text = "";
            labelErrorDoc.Visible = false;
        }
        private void textBoxCountPatDoc_Click(object sender, EventArgs e)
        {
            if (textBoxCountPatDoc.Text == "num")
                textBoxCountPatDoc.Text = "";
            labelErrorDoc.Visible = false;
        }
        private void textBoxCabDoc_Click(object sender, EventArgs e)
        {
            if (textBoxCabDoc.Text == "number")
                textBoxCabDoc.Text = "";
            labelErrorDoc.Visible = false;
        }


        private void textBoxSurnameDoc_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^[А-ЯЇЄІ][а-яїєі']{2,10}$", RegexOptions.Compiled);

            if (regex.IsMatch(textBoxSurnameDoc.Text))
                textBoxSurnameDoc.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxSurnameDoc.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxNameDoc_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^[А-ЯЇЄІ][а-яїєі']{2,12}$", RegexOptions.Compiled);

            if (regex.IsMatch(textBoxNameDoc.Text))
                textBoxNameDoc.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxNameDoc.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxPatronDoc_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^[А-ЯЇЄІ][а-яїєі']{6,12}$", RegexOptions.Compiled);

            if (regex.IsMatch(textBoxPatronDoc.Text))
                textBoxPatronDoc.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxPatronDoc.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxTelDoc_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^(\+?38)?0(39|50|6[36-8]|73|89|9[1-9])(\d{3}\d{2}\d{2})$", RegexOptions.Compiled);

            if (regex.IsMatch(textBoxTelDoc.Text))
                textBoxTelDoc.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxTelDoc.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxPositionDoc_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^[а-яієї\'\-]{6,20}$", RegexOptions.Compiled);

            if (regex.IsMatch(textBoxPositionDoc.Text))
                textBoxPositionDoc.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxPositionDoc.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxCountPatDoc_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^(([1-9][\d])|(0[1-9]))$", RegexOptions.Compiled);

            if (regex.IsMatch(textBoxCountPatDoc.Text))
                textBoxCountPatDoc.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxCountPatDoc.ForeColor = Color.FromArgb(255, 106, 106);
        }
        private void textBoxCabDoc_TextChanged(object sender, EventArgs e)
        {
            regex = new Regex(@"^(([1-9][\d])|(0[1-9]))$", RegexOptions.Compiled);

            if (regex.IsMatch(textBoxCabDoc.Text))
                textBoxCabDoc.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxCabDoc.ForeColor = Color.FromArgb(255, 106, 106);
        }

       
        
        private void buttonAddDoc_Click(object sender, EventArgs e)
        {
            if(textBoxNameDoc.ForeColor== Color.FromArgb(147, 255, 174)&& textBoxSurnameDoc.ForeColor== Color.FromArgb(147, 255, 174)&& textBoxPatronDoc.ForeColor== Color.FromArgb(147, 255, 174)&& textBoxTelDoc.ForeColor== Color.FromArgb(147, 255, 174)&& textBoxPositionDoc.ForeColor== Color.FromArgb(147, 255, 174)&&textBoxCountPatDoc.ForeColor== Color.FromArgb(147, 255, 174)&& textBoxCabDoc.ForeColor== Color.FromArgb(147, 255, 174))
            {
                if (textBoxCountPatDoc.Text[0] == '0')
                    textBoxCountPatDoc.Text = textBoxCountPatDoc.Text[1].ToString();
                if (textBoxCabDoc.Text[0] == '0')
                    textBoxCabDoc.Text = textBoxCabDoc.Text[1].ToString();
                
                Doctor doctor = new Doctor(textBoxSurnameDoc.Text, textBoxNameDoc.Text, textBoxPatronDoc.Text, textBoxTelDoc.Text, textBoxPositionDoc.Text, textBoxCountPatDoc.Text, textBoxCabDoc.Text);


                labelErrorDoc.Text = "";

                string[] allDoctors = File.ReadAllLines(PathToFiles.doctors);
               
                if (allDoctors.Length > 0)
                {
                    Regex regexNameCheck = new Regex($@"{doctor.Name}", RegexOptions.Compiled);
                    Regex regexSurnameCheck = new Regex($@"{doctor.Surname}", RegexOptions.Compiled);
                    Regex regexPatronymicCheck = new Regex($@"{doctor.Patronymic}", RegexOptions.Compiled);

                    for (int i = 0; i < allDoctors.Length; i++)
                    {
                        if (regexNameCheck.IsMatch(allDoctors[i]) && regexSurnameCheck.IsMatch(allDoctors[i]) && regexPatronymicCheck.IsMatch(allDoctors[i]))
                        {
                            labelErrorDoc.Text = "Цей лікар вже доданий";
                            labelError.Visible = true;
                        }
                    }
                    if (string.IsNullOrWhiteSpace(allDoctors[0]))
                        File.WriteAllText(PathToFiles.doctors, string.Empty);
                }

                if (allDoctors.Length == 0 || string.IsNullOrWhiteSpace(allDoctors[0]))
                {
                    string line = $"Прізвище:\tІм'я:\tПо-батькові:\tТелефон:\tПосада:\tМакс.Пацієнтів:\tКабінет:";

                    using (StreamWriter file = new StreamWriter(PathToFiles.doctors, true, Encoding.UTF8))
                        file.WriteLine(line);

                    dataGridViewPatients.Visible = false;
                }

                if (labelErrorDoc.Text != "Цей лікар вже доданий")
                {
                    string line = $"{doctor.Surname}\t{doctor.Name}\t{doctor.Patronymic}\t{doctor.Tel}\t{doctor.Position}\t{doctor.MaxCountPatients}\t{doctor.Cabinet}";

                    using (StreamWriter file = new StreamWriter(PathToFiles.doctors, true, Encoding.UTF8))
                        file.WriteLine(line);

                    textBoxNameDoc.Text = textBoxSurnameDoc.Text = textBoxPatronDoc.Text = textBoxTelDoc.Text = textBoxPositionDoc.Text = textBoxCountPatDoc.Text = textBoxCabDoc.Text = "";
                }
            }
            else
            {
                labelErrorDoc.Text = "Помилка у введені";
                labelErrorDoc.Visible = true;
            }
        }
        private void buttonShowDoc_Click(object sender, EventArgs e)
        {
            dataGridViewPatients.AllowUserToAddRows = false;
            dataGridViewPatients.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dataGridViewPatients.AllowUserToResizeColumns = false;
            dataGridViewPatients.AllowUserToResizeRows = false;
            dataGridViewPatients.Visible = true;
            labelErrorDoc.Visible = false;

            string[] allDoctors = File.ReadAllLines(PathToFiles.doctors);

            if (allDoctors.Length > 0)
            {
                DataTable table = new DataTable();

                string[] line = allDoctors[0].Split();
                for (int i = 0; i < line.Length; i++)
                {
                    line[i] = line[i].Trim();
                    if(i==5)
                        table.Columns.Add($"КількістьПацієнтів:", typeof(string));
                    table.Columns.Add($"{line[i]}", typeof(string));
                }

                for (int i = 1; i < allDoctors.Length; i++)
                {
                    line = allDoctors[i].Split();
                    for (int j = 0; j < line.Length; j++)
                    {
                        line[j] = line[j].Trim();
                    }

                    Doctor doctor = new Doctor(line[0], line[1], line[2], line[3], line[4], line[5], line[6]);

                    string[] allPatients = File.ReadAllLines(PathToFiles.patients);
                    int countPatients = 0;
                    if (allPatients.Length > 1) 
                    {
                        Regex regexCheck = new Regex($@"{doctor.Surname} {doctor.Name} {doctor.Patronymic}", RegexOptions.Compiled);
                        for (int l = 1; l < allPatients.Length; l++)
                            if (regexCheck.IsMatch(allPatients[l]))
                                countPatients++;
                    }
                    
                    table.Rows.Add(doctor.Surname, doctor.Name, doctor.Patronymic, doctor.Tel, doctor.Position, countPatients.ToString(), doctor.MaxCountPatients,doctor.Cabinet);
                }
                
                dataGridViewPatients.DataSource = table;
                
                for (int i = 0; i < 8; i++)
                {
                    dataGridViewPatients.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                }
            }
            if (allDoctors.Length == 1 || allDoctors.Length < 1)
            {
                dataGridViewPatients.Visible = false;
                labelErrorDoc.Text = "Лікарі відсутні";
                labelErrorDoc.Visible = true;
            }
        }
        private void buttonDeleteDoc_Click(object sender, EventArgs e)
        {
            string[] allDoctors = File.ReadAllLines(PathToFiles.doctors);

            if (dataGridViewPatients.Columns.Count == 8)
            {
                if (allDoctors.Length > 1)
                {
                    allDoctors[dataGridViewPatients.CurrentCell.RowIndex + 1] = "";
                    File.WriteAllText(PathToFiles.doctors, string.Empty);

                    using (StreamWriter writer = new StreamWriter(PathToFiles.doctors))
                    {
                        foreach (string str in allDoctors)
                        {
                            if (!string.IsNullOrWhiteSpace(str))
                                writer.WriteLine(str);
                        }
                    }

                    buttonShowDoc_Click(sender, e);
                }
                else
                {
                    labelError.Text = "Лікарі відсутні*";
                    labelError.Visible = true;
                }
            }
            else if(dataGridViewPatients.Columns.Count >0 && dataGridViewPatients.Columns.Count < 8)
            {
                labelErrorDoc.Text = "Лікарі відсутні*";
                labelErrorDoc.Visible = true;
            }
            else 
            {
                labelErrorDoc.Text = "Ви працюєте не\nз пацієнтами*";
                labelErrorDoc.Visible = true;
            }
        }




        private void DataForm_Load(object sender, EventArgs e)
        {
            labelPatien.ForeColor = Color.Black;
            labelDoc.ForeColor = Color.White;
            panelPatient.Visible = true;
            panelDoctor.Visible = false;
        }
        private void labelPatien_Click(object sender, EventArgs e)
        {
            labelPatien.ForeColor = Color.Black;
            labelDoc.ForeColor = Color.White;
            panelPatient.Visible = true;
            panelDoctor.Visible = false;
            dataGridViewPatients.Visible = false;
        }
        private void labelDoc_Click(object sender, EventArgs e)
        {
            labelPatien.ForeColor = Color.White;
            labelDoc.ForeColor = Color.Black;
            panelPatient.Visible = false;
            panelDoctor.Visible = true;
            dataGridViewPatients.Visible = false;
        }
    }
}
