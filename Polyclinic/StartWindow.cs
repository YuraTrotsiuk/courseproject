﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ClassLibraryPolyclinic;

namespace Polyclinic
{
    public partial class StartWindow : Form
    {
        public StartWindow()
        {
            InitializeComponent();
        }

        private void labelClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonLogIn_Click(object sender, EventArgs e)
        {
           
            LoginForm loginForm = new LoginForm();
            loginForm.ShowDialog();
            this.Close();
        }

        private void buttonRegistration_Click(object sender, EventArgs e)
        {
            RegistrationForm registrationForm = new RegistrationForm();
            registrationForm.ShowDialog();
            this.Close();
        }

        private void StartWindow_Load(object sender, EventArgs e)
        {
            PathToFiles.accounts = @"C:\Users\Acer\OneDrive\Робочий стіл\accounts.txt";
            PathToFiles.patients = @"C:\Users\Acer\OneDrive\Робочий стіл\patients.txt";
            PathToFiles.doctors = @"C:\Users\Acer\OneDrive\Робочий стіл\doctors.txt";


            if (!File.Exists(PathToFiles.accounts))
                File.Create(PathToFiles.accounts);
            
            if (!File.Exists(PathToFiles.patients))
                File.Create(PathToFiles.patients);
            
            if (!File.Exists(PathToFiles.doctors))
                File.Create(PathToFiles.doctors);
        }
    }
}
