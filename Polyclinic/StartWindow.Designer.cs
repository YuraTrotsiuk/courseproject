﻿namespace Polyclinic
{
    partial class StartWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNameProj = new System.Windows.Forms.Label();
            this.labelClose = new System.Windows.Forms.Label();
            this.labelPolyclinic = new System.Windows.Forms.Label();
            this.buttonLogIn = new System.Windows.Forms.Button();
            this.buttonRegistration = new System.Windows.Forms.Button();
            this.labelAuthor = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelNameProj
            // 
            this.labelNameProj.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelNameProj.ForeColor = System.Drawing.Color.White;
            this.labelNameProj.Location = new System.Drawing.Point(12, 172);
            this.labelNameProj.Name = "labelNameProj";
            this.labelNameProj.Size = new System.Drawing.Size(408, 35);
            this.labelNameProj.TabIndex = 0;
            this.labelNameProj.Text = "Інформаційно-пошукова система";
            this.labelNameProj.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelClose
            // 
            this.labelClose.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelClose.ForeColor = System.Drawing.SystemColors.Control;
            this.labelClose.Location = new System.Drawing.Point(3, -2);
            this.labelClose.Name = "labelClose";
            this.labelClose.Size = new System.Drawing.Size(429, 23);
            this.labelClose.TabIndex = 1;
            this.labelClose.Text = "x";
            this.labelClose.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelClose.Click += new System.EventHandler(this.labelClose_Click);
            // 
            // labelPolyclinic
            // 
            this.labelPolyclinic.Font = new System.Drawing.Font("Arial Black", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPolyclinic.ForeColor = System.Drawing.Color.White;
            this.labelPolyclinic.Location = new System.Drawing.Point(12, 113);
            this.labelPolyclinic.Name = "labelPolyclinic";
            this.labelPolyclinic.Size = new System.Drawing.Size(408, 59);
            this.labelPolyclinic.TabIndex = 2;
            this.labelPolyclinic.Text = "Поліклініка";
            this.labelPolyclinic.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonLogIn
            // 
            this.buttonLogIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLogIn.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonLogIn.ForeColor = System.Drawing.Color.White;
            this.buttonLogIn.Location = new System.Drawing.Point(40, 258);
            this.buttonLogIn.Name = "buttonLogIn";
            this.buttonLogIn.Size = new System.Drawing.Size(176, 35);
            this.buttonLogIn.TabIndex = 3;
            this.buttonLogIn.TabStop = false;
            this.buttonLogIn.Text = "Вхід";
            this.buttonLogIn.UseVisualStyleBackColor = true;
            this.buttonLogIn.Click += new System.EventHandler(this.buttonLogIn_Click);
            // 
            // buttonRegistration
            // 
            this.buttonRegistration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRegistration.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRegistration.ForeColor = System.Drawing.Color.White;
            this.buttonRegistration.Location = new System.Drawing.Point(222, 258);
            this.buttonRegistration.Name = "buttonRegistration";
            this.buttonRegistration.Size = new System.Drawing.Size(176, 35);
            this.buttonRegistration.TabIndex = 4;
            this.buttonRegistration.TabStop = false;
            this.buttonRegistration.Text = "Реєстрація";
            this.buttonRegistration.UseVisualStyleBackColor = true;
            this.buttonRegistration.Click += new System.EventHandler(this.buttonRegistration_Click);
            // 
            // labelAuthor
            // 
            this.labelAuthor.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.labelAuthor.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAuthor.ForeColor = System.Drawing.Color.White;
            this.labelAuthor.Location = new System.Drawing.Point(0, 456);
            this.labelAuthor.Name = "labelAuthor";
            this.labelAuthor.Size = new System.Drawing.Size(432, 36);
            this.labelAuthor.TabIndex = 5;
            this.labelAuthor.Text = "Copyright Yriy Trotsiuk";
            this.labelAuthor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // StartWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.ClientSize = new System.Drawing.Size(432, 492);
            this.Controls.Add(this.labelAuthor);
            this.Controls.Add(this.buttonRegistration);
            this.Controls.Add(this.buttonLogIn);
            this.Controls.Add(this.labelPolyclinic);
            this.Controls.Add(this.labelClose);
            this.Controls.Add(this.labelNameProj);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "StartWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Поліклініка";
            this.Load += new System.EventHandler(this.StartWindow_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelNameProj;
        private System.Windows.Forms.Label labelClose;
        private System.Windows.Forms.Label labelPolyclinic;
        private System.Windows.Forms.Button buttonLogIn;
        private System.Windows.Forms.Button buttonRegistration;
        private System.Windows.Forms.Label labelAuthor;
    }
}

