﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.IO;
using ClassLibraryPolyclinic;

namespace Polyclinic
{
    public partial class RegistrationForm : Form
    {
        public RegistrationForm()
        {
            InitializeComponent();
        }

        private void labelClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBoxTel_Click(object sender, EventArgs e)
        {
            if (textBoxTel.Text == "Tel")
                textBoxTel.Text = "";
        }

        private void textBoxLogin_Click(object sender, EventArgs e)
        {
            if (textBoxLogin.Text == "Login")
                textBoxLogin.Text = "";
        }

        private void textBoxPassword_Click(object sender, EventArgs e)
        {
            if (textBoxPassword.Text == "Password")
            {
                textBoxPassword.Text = "";
                textBoxPassword.UseSystemPasswordChar = true;
            }
        }
    
        Regex regexTel = new Regex(@"^(\+?38)?0(39|50|6[36-8]|73|89|9[1-9])(\d{3}\d{2}\d{2})$", RegexOptions.Compiled);
        Regex regexLogin = new Regex (@"^[a-zA-Z\d]{3,25}$", RegexOptions.Compiled);
        Regex regexPassword = new Regex(@"^[a-zA-Z\d]{5,10}$", RegexOptions.Compiled);
        private void textBoxTel_TextChanged(object sender, EventArgs e)
        {
            labelError.Visible = false;
            if (regexTel.IsMatch(textBoxTel.Text))
                textBoxTel.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxTel.ForeColor = Color.FromArgb(255, 106, 106);
        }

        private void textBoxLogin_TextChanged(object sender, EventArgs e)
        {
            labelError.Visible = false;
            if (regexLogin.IsMatch(textBoxLogin.Text))
                textBoxLogin.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxLogin.ForeColor = Color.FromArgb(255, 106, 106);
        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {
            labelError.Visible = false;
            if (regexPassword.IsMatch(textBoxPassword.Text))
                textBoxPassword.ForeColor = Color.FromArgb(147, 255, 174);
            else
                textBoxPassword.ForeColor = Color.FromArgb(255, 106, 106);
        }

        private void buttonLogIn_Click(object sender, EventArgs e)
        {
            if (textBoxTel.ForeColor == Color.FromArgb(147, 255, 174) && textBoxLogin.ForeColor == Color.FromArgb(147, 255, 174) && textBoxPassword.ForeColor == Color.FromArgb(147, 255, 174))
            {
                Account account = new Account(textBoxTel.Text, textBoxLogin.Text, textBoxPassword.Text);

                labelError.Text = "";
                
                string[] allAccounts = File.ReadAllLines(PathToFiles.accounts);
                
                Regex regexLoginCheck = new Regex($@"{account.Login}", RegexOptions.Compiled);

                string regexTelPattarn = account.Tel ;
                if (account.Tel[0] == '+')
                    regexTelPattarn = regexTelPattarn.Remove(0, 3);
                if (account.Tel[0] == '3')
                    regexTelPattarn = regexTelPattarn.Remove(0, 2);
                Regex regexTelCheck = new Regex($@"{regexTelPattarn}", RegexOptions.Compiled);

                if (allAccounts.Length > 0)
                {
                    for (int i = 0; i < allAccounts.Length; i++)
                    {                        
                        if (regexTelCheck.IsMatch(allAccounts[i]))
                        {
                            labelError.Text = "Цей номер вже зайнятий!";
                            labelError.Visible = true;
                        }
                        if (regexLoginCheck.IsMatch(allAccounts[i]))
                        {   
                            labelError.Text = "Цей логін вже зайнятий!";
                            labelError.Visible = true;
                        }
                        if (regexLoginCheck.IsMatch(allAccounts[i]) && regexTelCheck.IsMatch(allAccounts[i]))
                        {
                            labelError.Text = "Цей логін вже зайнятий!";
                            labelError.Visible = true;
                        }
                    }
                }
                if (labelError.Text != "Цей номер вже зайнятий!" && labelError.Text != "Цей логін вже зайнятий!")
                {
                    string line = $"Tel: {account.Tel}\t Login: {account.Login}\t Password: {account.Password}";

                    using (StreamWriter file = new StreamWriter(PathToFiles.accounts, true, Encoding.UTF8))
                        file.WriteLine(line);

                    StartWindow startWindow = new StartWindow();
                    startWindow.ShowDialog();
                    this.Close();
                }
            }
            else
            {
                labelError.Text = "Помилка у введені даних!";
                labelError.Visible = true;
            }           
        }

        private void RegistrationForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoginForm loginForm = new LoginForm();
            loginForm.ShowDialog();
            this.Close();
        }
    }
}
