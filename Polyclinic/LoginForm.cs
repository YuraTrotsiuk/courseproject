﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using ClassLibraryPolyclinic;

namespace Polyclinic
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void labelClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBoxUserName_Click(object sender, EventArgs e)
        {
            if (textBoxLogin.Text == "Login")
            {
                textBoxLogin.Text = "";
                textBoxLogin.ForeColor = Color.White;
            }
        }

        private void textBoxPassword_Click(object sender, EventArgs e)
        {
            if (textBoxPassword.Text == "Password")
            { 
                textBoxPassword.Text = ""; 
                textBoxPassword.ForeColor = Color.White;
            }

        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            
        }

        private void buttonRegistration_Click(object sender, EventArgs e)
        {
            RegistrationForm registrationForm = new RegistrationForm();
            registrationForm.ShowDialog();
            this.Close();
        }

        private void buttonLogIn_Click(object sender, EventArgs e)
        {
            int flagOne,flagTwo;
            flagOne = -1;
            flagTwo = -2;

            if (textBoxLogin.Text.Length > 0 && textBoxPassword.Text.Length > 0)
            {
                User user = new User(textBoxLogin.Text, textBoxPassword.Text);
                string[] allAccounts = File.ReadAllLines(PathToFiles.accounts);
                if (allAccounts.Length > 0)
                {

                    for (int i = 0; i < allAccounts.Length; i++)
                    {
                        string[] line = allAccounts[i].Split(' ');
                        for (int j = 0; j < line.Length; j++)
                        {
                            line[j] = line[j].Trim();
                            if (user.Login == line[j])
                                flagOne = i;
                            if (user.Password == line[j])
                            {
                                flagTwo = i;
                            }
                        }
                        if (flagOne == flagTwo)
                        {
                            DataForm dataForm = new DataForm();
                            dataForm.ShowDialog();           
                            this.Close();
                        }
                        else
                        {
                            labelError.Text = "Логін чи пароль невірний*";
                            labelError.Visible = true;
                        }
                    }
                }
                else
                {
                    labelError.Text = "Логін чи пароль невірний*";
                    labelError.Visible = true;
                }
            }
            else
            { 
                labelError.Text = "Введіть логін та пароль*";
                labelError.Visible = true;
            }



        }

        private void textBoxLogin_TextChanged(object sender, EventArgs e)
        {
            labelError.Visible = false;
        }

        private void textBoxPassword_TextChanged(object sender, EventArgs e)
        {
            labelError.Visible = false;
            textBoxPassword.UseSystemPasswordChar = true;
        }
    }
}
