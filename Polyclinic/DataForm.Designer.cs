﻿namespace Polyclinic
{
    partial class DataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataForm));
            this.labelClose = new System.Windows.Forms.Label();
            this.labelName = new System.Windows.Forms.Label();
            this.labelSurname = new System.Windows.Forms.Label();
            this.labelPatronymic = new System.Windows.Forms.Label();
            this.labelTel = new System.Windows.Forms.Label();
            this.labelPassport = new System.Windows.Forms.Label();
            this.labelDoctor = new System.Windows.Forms.Label();
            this.panelPatient = new System.Windows.Forms.Panel();
            this.labelError = new System.Windows.Forms.Label();
            this.buttonShowPatients = new System.Windows.Forms.Button();
            this.buttonRemovePatient = new System.Windows.Forms.Button();
            this.textBoxName = new System.Windows.Forms.TextBox();
            this.buttonAddPatient = new System.Windows.Forms.Button();
            this.labelBirthday = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.textBoxBirthday = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textBoxDoctor = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textBoxPassport = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.textBoxTel = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.textBoxPatronymic = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.textBoxSurname = new System.Windows.Forms.TextBox();
            this.panelTel = new System.Windows.Forms.Panel();
            this.dataGridViewPatients = new System.Windows.Forms.DataGridView();
            this.panelDoctor = new System.Windows.Forms.Panel();
            this.textBoxNameDoc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel8 = new System.Windows.Forms.Panel();
            this.textBoxPatronDoc = new System.Windows.Forms.TextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.textBoxSurnameDoc = new System.Windows.Forms.TextBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.labelErrorDoc = new System.Windows.Forms.Label();
            this.buttonShowDoc = new System.Windows.Forms.Button();
            this.buttonDeleteDoc = new System.Windows.Forms.Button();
            this.buttonAddDoc = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.textBoxCabDoc = new System.Windows.Forms.TextBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.textBoxCountPatDoc = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel9 = new System.Windows.Forms.Panel();
            this.textBoxPositionDoc = new System.Windows.Forms.TextBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.textBoxTelDoc = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelPatien = new System.Windows.Forms.Label();
            this.labelDoc = new System.Windows.Forms.Label();
            this.labelAuthor = new System.Windows.Forms.Label();
            this.panelPatient.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPatients)).BeginInit();
            this.panelDoctor.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelClose
            // 
            this.labelClose.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelClose.ForeColor = System.Drawing.SystemColors.Control;
            this.labelClose.Location = new System.Drawing.Point(372, -2);
            this.labelClose.Name = "labelClose";
            this.labelClose.Size = new System.Drawing.Size(429, 22);
            this.labelClose.TabIndex = 23;
            this.labelClose.Text = "x";
            this.labelClose.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.labelClose.Click += new System.EventHandler(this.labelClose_Click);
            // 
            // labelName
            // 
            this.labelName.AutoSize = true;
            this.labelName.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelName.ForeColor = System.Drawing.Color.White;
            this.labelName.Location = new System.Drawing.Point(12, 61);
            this.labelName.Name = "labelName";
            this.labelName.Size = new System.Drawing.Size(42, 19);
            this.labelName.TabIndex = 24;
            this.labelName.Text = "Ім\'я:";
            // 
            // labelSurname
            // 
            this.labelSurname.AutoSize = true;
            this.labelSurname.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelSurname.ForeColor = System.Drawing.Color.White;
            this.labelSurname.Location = new System.Drawing.Point(12, 22);
            this.labelSurname.Name = "labelSurname";
            this.labelSurname.Size = new System.Drawing.Size(87, 19);
            this.labelSurname.TabIndex = 25;
            this.labelSurname.Text = "Прізвище:";
            // 
            // labelPatronymic
            // 
            this.labelPatronymic.AutoSize = true;
            this.labelPatronymic.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPatronymic.ForeColor = System.Drawing.Color.White;
            this.labelPatronymic.Location = new System.Drawing.Point(12, 101);
            this.labelPatronymic.Name = "labelPatronymic";
            this.labelPatronymic.Size = new System.Drawing.Size(104, 19);
            this.labelPatronymic.TabIndex = 26;
            this.labelPatronymic.Text = "По-батькові:";
            // 
            // labelTel
            // 
            this.labelTel.AutoSize = true;
            this.labelTel.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelTel.ForeColor = System.Drawing.Color.White;
            this.labelTel.Location = new System.Drawing.Point(12, 181);
            this.labelTel.Name = "labelTel";
            this.labelTel.Size = new System.Drawing.Size(80, 19);
            this.labelTel.TabIndex = 27;
            this.labelTel.Text = "Телефон:";
            // 
            // labelPassport
            // 
            this.labelPassport.AutoSize = true;
            this.labelPassport.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPassport.ForeColor = System.Drawing.Color.White;
            this.labelPassport.Location = new System.Drawing.Point(14, 221);
            this.labelPassport.Name = "labelPassport";
            this.labelPassport.Size = new System.Drawing.Size(78, 19);
            this.labelPassport.TabIndex = 28;
            this.labelPassport.Text = "Паспорт:";
            // 
            // labelDoctor
            // 
            this.labelDoctor.AutoSize = true;
            this.labelDoctor.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDoctor.ForeColor = System.Drawing.Color.White;
            this.labelDoctor.Location = new System.Drawing.Point(14, 261);
            this.labelDoctor.Name = "labelDoctor";
            this.labelDoctor.Size = new System.Drawing.Size(53, 19);
            this.labelDoctor.TabIndex = 30;
            this.labelDoctor.Text = "Лікар:";
            // 
            // panelPatient
            // 
            this.panelPatient.Controls.Add(this.labelError);
            this.panelPatient.Controls.Add(this.buttonShowPatients);
            this.panelPatient.Controls.Add(this.buttonRemovePatient);
            this.panelPatient.Controls.Add(this.textBoxName);
            this.panelPatient.Controls.Add(this.labelName);
            this.panelPatient.Controls.Add(this.buttonAddPatient);
            this.panelPatient.Controls.Add(this.labelBirthday);
            this.panelPatient.Controls.Add(this.panel7);
            this.panelPatient.Controls.Add(this.textBoxBirthday);
            this.panelPatient.Controls.Add(this.panel6);
            this.panelPatient.Controls.Add(this.textBoxDoctor);
            this.panelPatient.Controls.Add(this.panel4);
            this.panelPatient.Controls.Add(this.textBoxPassport);
            this.panelPatient.Controls.Add(this.panel3);
            this.panelPatient.Controls.Add(this.textBoxTel);
            this.panelPatient.Controls.Add(this.panel2);
            this.panelPatient.Controls.Add(this.textBoxPatronymic);
            this.panelPatient.Controls.Add(this.panel1);
            this.panelPatient.Controls.Add(this.textBoxSurname);
            this.panelPatient.Controls.Add(this.panelTel);
            this.panelPatient.Controls.Add(this.labelDoctor);
            this.panelPatient.Controls.Add(this.labelSurname);
            this.panelPatient.Controls.Add(this.labelPatronymic);
            this.panelPatient.Controls.Add(this.labelPassport);
            this.panelPatient.Controls.Add(this.labelTel);
            this.panelPatient.Location = new System.Drawing.Point(561, 55);
            this.panelPatient.Name = "panelPatient";
            this.panelPatient.Size = new System.Drawing.Size(239, 437);
            this.panelPatient.TabIndex = 31;
            // 
            // labelError
            // 
            this.labelError.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelError.ForeColor = System.Drawing.Color.Gold;
            this.labelError.Image = ((System.Drawing.Image)(resources.GetObject("labelError.Image")));
            this.labelError.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelError.Location = new System.Drawing.Point(3, 287);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(239, 61);
            this.labelError.TabIndex = 51;
            this.labelError.Text = "Цей пацієнт вже доданий*";
            this.labelError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelError.Visible = false;
            // 
            // buttonShowPatients
            // 
            this.buttonShowPatients.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowPatients.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonShowPatients.ForeColor = System.Drawing.Color.White;
            this.buttonShowPatients.Location = new System.Drawing.Point(7, 351);
            this.buttonShowPatients.Name = "buttonShowPatients";
            this.buttonShowPatients.Size = new System.Drawing.Size(226, 35);
            this.buttonShowPatients.TabIndex = 50;
            this.buttonShowPatients.TabStop = false;
            this.buttonShowPatients.Text = "Показати пацієтів";
            this.buttonShowPatients.UseVisualStyleBackColor = true;
            this.buttonShowPatients.Click += new System.EventHandler(this.buttonShowPatients_Click);
            // 
            // buttonRemovePatient
            // 
            this.buttonRemovePatient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonRemovePatient.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonRemovePatient.ForeColor = System.Drawing.Color.White;
            this.buttonRemovePatient.Location = new System.Drawing.Point(121, 392);
            this.buttonRemovePatient.Name = "buttonRemovePatient";
            this.buttonRemovePatient.Size = new System.Drawing.Size(112, 35);
            this.buttonRemovePatient.TabIndex = 49;
            this.buttonRemovePatient.TabStop = false;
            this.buttonRemovePatient.Text = "Видалити";
            this.buttonRemovePatient.UseVisualStyleBackColor = true;
            this.buttonRemovePatient.Click += new System.EventHandler(this.buttonRemovePatient_Click);
            // 
            // textBoxName
            // 
            this.textBoxName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxName.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxName.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxName.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxName.Location = new System.Drawing.Point(60, 61);
            this.textBoxName.Name = "textBoxName";
            this.textBoxName.Size = new System.Drawing.Size(150, 20);
            this.textBoxName.TabIndex = 31;
            this.textBoxName.TabStop = false;
            this.textBoxName.Text = "name";
            this.textBoxName.Click += new System.EventHandler(this.textBoxName_Click);
            this.textBoxName.TextChanged += new System.EventHandler(this.textBoxName_TextChanged);
            // 
            // buttonAddPatient
            // 
            this.buttonAddPatient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddPatient.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddPatient.ForeColor = System.Drawing.Color.White;
            this.buttonAddPatient.Location = new System.Drawing.Point(7, 392);
            this.buttonAddPatient.Name = "buttonAddPatient";
            this.buttonAddPatient.Size = new System.Drawing.Size(112, 35);
            this.buttonAddPatient.TabIndex = 48;
            this.buttonAddPatient.TabStop = false;
            this.buttonAddPatient.Text = "Додати";
            this.buttonAddPatient.UseVisualStyleBackColor = true;
            this.buttonAddPatient.Click += new System.EventHandler(this.buttonAddPatient_Click);
            // 
            // labelBirthday
            // 
            this.labelBirthday.AutoSize = true;
            this.labelBirthday.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelBirthday.ForeColor = System.Drawing.Color.White;
            this.labelBirthday.Location = new System.Drawing.Point(15, 141);
            this.labelBirthday.Name = "labelBirthday";
            this.labelBirthday.Size = new System.Drawing.Size(84, 19);
            this.labelBirthday.TabIndex = 47;
            this.labelBirthday.Text = "Дата нар:";
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel7.Location = new System.Drawing.Point(16, 163);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(200, 1);
            this.panel7.TabIndex = 46;
            // 
            // textBoxBirthday
            // 
            this.textBoxBirthday.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxBirthday.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxBirthday.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxBirthday.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxBirthday.Location = new System.Drawing.Point(113, 141);
            this.textBoxBirthday.Name = "textBoxBirthday";
            this.textBoxBirthday.Size = new System.Drawing.Size(110, 20);
            this.textBoxBirthday.TabIndex = 45;
            this.textBoxBirthday.TabStop = false;
            this.textBoxBirthday.Text = "birthday";
            this.textBoxBirthday.Click += new System.EventHandler(this.textBoxBirthday_Click);
            this.textBoxBirthday.TextChanged += new System.EventHandler(this.textBoxBirthday_TextChanged);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel6.Location = new System.Drawing.Point(18, 283);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(200, 1);
            this.panel6.TabIndex = 44;
            // 
            // textBoxDoctor
            // 
            this.textBoxDoctor.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxDoctor.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxDoctor.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxDoctor.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxDoctor.Location = new System.Drawing.Point(74, 260);
            this.textBoxDoctor.Name = "textBoxDoctor";
            this.textBoxDoctor.Size = new System.Drawing.Size(150, 20);
            this.textBoxDoctor.TabIndex = 43;
            this.textBoxDoctor.TabStop = false;
            this.textBoxDoctor.Text = "doctor";
            this.textBoxDoctor.Click += new System.EventHandler(this.textBoxDoctor_Click);
            this.textBoxDoctor.TextChanged += new System.EventHandler(this.textBoxDoctor_TextChanged);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel4.Location = new System.Drawing.Point(16, 243);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(200, 1);
            this.panel4.TabIndex = 40;
            // 
            // textBoxPassport
            // 
            this.textBoxPassport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxPassport.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPassport.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxPassport.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxPassport.Location = new System.Drawing.Point(104, 220);
            this.textBoxPassport.Name = "textBoxPassport";
            this.textBoxPassport.Size = new System.Drawing.Size(110, 20);
            this.textBoxPassport.TabIndex = 39;
            this.textBoxPassport.TabStop = false;
            this.textBoxPassport.Text = "number pass";
            this.textBoxPassport.Click += new System.EventHandler(this.textBoxPassport_Click);
            this.textBoxPassport.TextChanged += new System.EventHandler(this.textBoxPassport_TextChanged);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel3.Location = new System.Drawing.Point(16, 203);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 1);
            this.panel3.TabIndex = 38;
            // 
            // textBoxTel
            // 
            this.textBoxTel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxTel.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxTel.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxTel.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxTel.Location = new System.Drawing.Point(104, 180);
            this.textBoxTel.Name = "textBoxTel";
            this.textBoxTel.Size = new System.Drawing.Size(125, 20);
            this.textBoxTel.TabIndex = 37;
            this.textBoxTel.TabStop = false;
            this.textBoxTel.Text = "number tel";
            this.textBoxTel.Click += new System.EventHandler(this.textBoxTel_Click);
            this.textBoxTel.TextChanged += new System.EventHandler(this.textBoxTel_TextChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel2.Location = new System.Drawing.Point(16, 123);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 1);
            this.panel2.TabIndex = 36;
            // 
            // textBoxPatronymic
            // 
            this.textBoxPatronymic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxPatronymic.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPatronymic.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxPatronymic.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxPatronymic.Location = new System.Drawing.Point(128, 101);
            this.textBoxPatronymic.Name = "textBoxPatronymic";
            this.textBoxPatronymic.Size = new System.Drawing.Size(95, 20);
            this.textBoxPatronymic.TabIndex = 35;
            this.textBoxPatronymic.TabStop = false;
            this.textBoxPatronymic.Text = "patronymic";
            this.textBoxPatronymic.Click += new System.EventHandler(this.textBoxPatronymic_Click);
            this.textBoxPatronymic.TextChanged += new System.EventHandler(this.textBoxPatronymic_TextChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel1.Location = new System.Drawing.Point(16, 83);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 1);
            this.panel1.TabIndex = 34;
            // 
            // textBoxSurname
            // 
            this.textBoxSurname.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxSurname.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxSurname.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxSurname.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxSurname.Location = new System.Drawing.Point(109, 21);
            this.textBoxSurname.Name = "textBoxSurname";
            this.textBoxSurname.Size = new System.Drawing.Size(120, 20);
            this.textBoxSurname.TabIndex = 33;
            this.textBoxSurname.TabStop = false;
            this.textBoxSurname.Text = "surname";
            this.textBoxSurname.Click += new System.EventHandler(this.textBoxSurname_Click);
            this.textBoxSurname.TextChanged += new System.EventHandler(this.textBoxSurname_TextChanged);
            // 
            // panelTel
            // 
            this.panelTel.BackColor = System.Drawing.Color.White;
            this.panelTel.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panelTel.Location = new System.Drawing.Point(16, 43);
            this.panelTel.Name = "panelTel";
            this.panelTel.Size = new System.Drawing.Size(200, 1);
            this.panelTel.TabIndex = 32;
            // 
            // dataGridViewPatients
            // 
            this.dataGridViewPatients.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.dataGridViewPatients.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewPatients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPatients.Location = new System.Drawing.Point(12, 55);
            this.dataGridViewPatients.Name = "dataGridViewPatients";
            this.dataGridViewPatients.RowHeadersWidth = 51;
            this.dataGridViewPatients.RowTemplate.Height = 24;
            this.dataGridViewPatients.Size = new System.Drawing.Size(543, 398);
            this.dataGridViewPatients.TabIndex = 32;
            // 
            // panelDoctor
            // 
            this.panelDoctor.Controls.Add(this.textBoxNameDoc);
            this.panelDoctor.Controls.Add(this.label1);
            this.panelDoctor.Controls.Add(this.panel8);
            this.panelDoctor.Controls.Add(this.textBoxPatronDoc);
            this.panelDoctor.Controls.Add(this.panel13);
            this.panelDoctor.Controls.Add(this.textBoxSurnameDoc);
            this.panelDoctor.Controls.Add(this.panel14);
            this.panelDoctor.Controls.Add(this.label3);
            this.panelDoctor.Controls.Add(this.label5);
            this.panelDoctor.Controls.Add(this.labelErrorDoc);
            this.panelDoctor.Controls.Add(this.buttonShowDoc);
            this.panelDoctor.Controls.Add(this.buttonDeleteDoc);
            this.panelDoctor.Controls.Add(this.buttonAddDoc);
            this.panelDoctor.Controls.Add(this.panel10);
            this.panelDoctor.Controls.Add(this.textBoxCabDoc);
            this.panelDoctor.Controls.Add(this.panel12);
            this.panelDoctor.Controls.Add(this.textBoxCountPatDoc);
            this.panelDoctor.Controls.Add(this.label2);
            this.panelDoctor.Controls.Add(this.label6);
            this.panelDoctor.Controls.Add(this.panel9);
            this.panelDoctor.Controls.Add(this.textBoxPositionDoc);
            this.panelDoctor.Controls.Add(this.panel11);
            this.panelDoctor.Controls.Add(this.textBoxTelDoc);
            this.panelDoctor.Controls.Add(this.label4);
            this.panelDoctor.Controls.Add(this.label8);
            this.panelDoctor.Location = new System.Drawing.Point(561, 55);
            this.panelDoctor.Name = "panelDoctor";
            this.panelDoctor.Size = new System.Drawing.Size(239, 437);
            this.panelDoctor.TabIndex = 33;
            this.panelDoctor.TabStop = true;
            // 
            // textBoxNameDoc
            // 
            this.textBoxNameDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxNameDoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxNameDoc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxNameDoc.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxNameDoc.Location = new System.Drawing.Point(60, 62);
            this.textBoxNameDoc.Name = "textBoxNameDoc";
            this.textBoxNameDoc.Size = new System.Drawing.Size(150, 20);
            this.textBoxNameDoc.TabIndex = 65;
            this.textBoxNameDoc.TabStop = false;
            this.textBoxNameDoc.Text = "name";
            this.textBoxNameDoc.Click += new System.EventHandler(this.textBoxNameDoc_Click);
            this.textBoxNameDoc.TextChanged += new System.EventHandler(this.textBoxNameDoc_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 19);
            this.label1.TabIndex = 62;
            this.label1.Text = "Ім\'я:";
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel8.Location = new System.Drawing.Point(16, 124);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(200, 1);
            this.panel8.TabIndex = 70;
            // 
            // textBoxPatronDoc
            // 
            this.textBoxPatronDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxPatronDoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPatronDoc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxPatronDoc.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxPatronDoc.Location = new System.Drawing.Point(128, 102);
            this.textBoxPatronDoc.Name = "textBoxPatronDoc";
            this.textBoxPatronDoc.Size = new System.Drawing.Size(95, 20);
            this.textBoxPatronDoc.TabIndex = 69;
            this.textBoxPatronDoc.TabStop = false;
            this.textBoxPatronDoc.Text = "patronymic";
            this.textBoxPatronDoc.Click += new System.EventHandler(this.textBoxPatronDoc_Click);
            this.textBoxPatronDoc.TextChanged += new System.EventHandler(this.textBoxPatronDoc_TextChanged);
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.Color.White;
            this.panel13.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel13.Location = new System.Drawing.Point(16, 84);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(200, 1);
            this.panel13.TabIndex = 68;
            // 
            // textBoxSurnameDoc
            // 
            this.textBoxSurnameDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxSurnameDoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxSurnameDoc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxSurnameDoc.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxSurnameDoc.Location = new System.Drawing.Point(109, 22);
            this.textBoxSurnameDoc.Name = "textBoxSurnameDoc";
            this.textBoxSurnameDoc.Size = new System.Drawing.Size(120, 20);
            this.textBoxSurnameDoc.TabIndex = 67;
            this.textBoxSurnameDoc.TabStop = false;
            this.textBoxSurnameDoc.Text = "surname";
            this.textBoxSurnameDoc.Click += new System.EventHandler(this.textBoxSurnameDoc_Click);
            this.textBoxSurnameDoc.TextChanged += new System.EventHandler(this.textBoxSurnameDoc_TextChanged);
            // 
            // panel14
            // 
            this.panel14.BackColor = System.Drawing.Color.White;
            this.panel14.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel14.Location = new System.Drawing.Point(16, 44);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(200, 1);
            this.panel14.TabIndex = 66;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(12, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 19);
            this.label3.TabIndex = 63;
            this.label3.Text = "Прізвище:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(12, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 19);
            this.label5.TabIndex = 64;
            this.label5.Text = "По-батькові:";
            // 
            // labelErrorDoc
            // 
            this.labelErrorDoc.Font = new System.Drawing.Font("Arial Narrow", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelErrorDoc.ForeColor = System.Drawing.Color.Gold;
            this.labelErrorDoc.Image = ((System.Drawing.Image)(resources.GetObject("labelErrorDoc.Image")));
            this.labelErrorDoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelErrorDoc.Location = new System.Drawing.Point(3, 283);
            this.labelErrorDoc.Name = "labelErrorDoc";
            this.labelErrorDoc.Size = new System.Drawing.Size(239, 65);
            this.labelErrorDoc.TabIndex = 61;
            this.labelErrorDoc.Text = "Цей лікар вже доданий*";
            this.labelErrorDoc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelErrorDoc.Visible = false;
            // 
            // buttonShowDoc
            // 
            this.buttonShowDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonShowDoc.Font = new System.Drawing.Font("Arial", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonShowDoc.ForeColor = System.Drawing.Color.White;
            this.buttonShowDoc.Location = new System.Drawing.Point(7, 351);
            this.buttonShowDoc.Name = "buttonShowDoc";
            this.buttonShowDoc.Size = new System.Drawing.Size(226, 35);
            this.buttonShowDoc.TabIndex = 60;
            this.buttonShowDoc.TabStop = false;
            this.buttonShowDoc.Text = "Показати лікарів";
            this.buttonShowDoc.UseVisualStyleBackColor = true;
            this.buttonShowDoc.Click += new System.EventHandler(this.buttonShowDoc_Click);
            // 
            // buttonDeleteDoc
            // 
            this.buttonDeleteDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDeleteDoc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonDeleteDoc.ForeColor = System.Drawing.Color.White;
            this.buttonDeleteDoc.Location = new System.Drawing.Point(121, 392);
            this.buttonDeleteDoc.Name = "buttonDeleteDoc";
            this.buttonDeleteDoc.Size = new System.Drawing.Size(112, 35);
            this.buttonDeleteDoc.TabIndex = 59;
            this.buttonDeleteDoc.TabStop = false;
            this.buttonDeleteDoc.Text = "Видалити";
            this.buttonDeleteDoc.UseVisualStyleBackColor = true;
            this.buttonDeleteDoc.Click += new System.EventHandler(this.buttonDeleteDoc_Click);
            // 
            // buttonAddDoc
            // 
            this.buttonAddDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAddDoc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.buttonAddDoc.ForeColor = System.Drawing.Color.White;
            this.buttonAddDoc.Location = new System.Drawing.Point(7, 392);
            this.buttonAddDoc.Name = "buttonAddDoc";
            this.buttonAddDoc.Size = new System.Drawing.Size(112, 35);
            this.buttonAddDoc.TabIndex = 58;
            this.buttonAddDoc.TabStop = false;
            this.buttonAddDoc.Text = "Додати";
            this.buttonAddDoc.UseVisualStyleBackColor = true;
            this.buttonAddDoc.Click += new System.EventHandler(this.buttonAddDoc_Click);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel10.Location = new System.Drawing.Point(17, 281);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(200, 1);
            this.panel10.TabIndex = 57;
            // 
            // textBoxCabDoc
            // 
            this.textBoxCabDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxCabDoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxCabDoc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxCabDoc.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxCabDoc.Location = new System.Drawing.Point(104, 259);
            this.textBoxCabDoc.Name = "textBoxCabDoc";
            this.textBoxCabDoc.Size = new System.Drawing.Size(110, 20);
            this.textBoxCabDoc.TabIndex = 56;
            this.textBoxCabDoc.TabStop = false;
            this.textBoxCabDoc.Text = "number";
            this.textBoxCabDoc.Click += new System.EventHandler(this.textBoxCabDoc_Click);
            this.textBoxCabDoc.TextChanged += new System.EventHandler(this.textBoxCabDoc_TextChanged);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel12.Location = new System.Drawing.Point(15, 243);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(200, 1);
            this.panel12.TabIndex = 55;
            // 
            // textBoxCountPatDoc
            // 
            this.textBoxCountPatDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxCountPatDoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxCountPatDoc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxCountPatDoc.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxCountPatDoc.Location = new System.Drawing.Point(165, 223);
            this.textBoxCountPatDoc.Name = "textBoxCountPatDoc";
            this.textBoxCountPatDoc.Size = new System.Drawing.Size(50, 20);
            this.textBoxCountPatDoc.TabIndex = 54;
            this.textBoxCountPatDoc.TabStop = false;
            this.textBoxCountPatDoc.Text = "num";
            this.textBoxCountPatDoc.Click += new System.EventHandler(this.textBoxCountPatDoc_Click);
            this.textBoxCountPatDoc.TextChanged += new System.EventHandler(this.textBoxCountPatDoc_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(15, 259);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 19);
            this.label2.TabIndex = 53;
            this.label2.Text = "Кабінет:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(15, 220);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(134, 19);
            this.label6.TabIndex = 52;
            this.label6.Text = "Макс. к-сть пац:";
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel9.Location = new System.Drawing.Point(15, 201);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(200, 1);
            this.panel9.TabIndex = 44;
            // 
            // textBoxPositionDoc
            // 
            this.textBoxPositionDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxPositionDoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPositionDoc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxPositionDoc.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxPositionDoc.Location = new System.Drawing.Point(88, 178);
            this.textBoxPositionDoc.Name = "textBoxPositionDoc";
            this.textBoxPositionDoc.Size = new System.Drawing.Size(135, 20);
            this.textBoxPositionDoc.TabIndex = 43;
            this.textBoxPositionDoc.TabStop = false;
            this.textBoxPositionDoc.Text = "position";
            this.textBoxPositionDoc.Click += new System.EventHandler(this.textBoxPositionDoc_Click);
            this.textBoxPositionDoc.TextChanged += new System.EventHandler(this.textBoxPositionDoc_TextChanged);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.White;
            this.panel11.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel11.Location = new System.Drawing.Point(16, 162);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(200, 1);
            this.panel11.TabIndex = 38;
            // 
            // textBoxTelDoc
            // 
            this.textBoxTelDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.textBoxTelDoc.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxTelDoc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxTelDoc.ForeColor = System.Drawing.Color.LightGray;
            this.textBoxTelDoc.Location = new System.Drawing.Point(104, 139);
            this.textBoxTelDoc.Name = "textBoxTelDoc";
            this.textBoxTelDoc.Size = new System.Drawing.Size(125, 20);
            this.textBoxTelDoc.TabIndex = 37;
            this.textBoxTelDoc.TabStop = false;
            this.textBoxTelDoc.Text = "number tel";
            this.textBoxTelDoc.Click += new System.EventHandler(this.textBoxTelDoc_Click);
            this.textBoxTelDoc.TextChanged += new System.EventHandler(this.textBoxTelDoc_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(13, 180);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 19);
            this.label4.TabIndex = 30;
            this.label4.Text = "Посада:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(12, 140);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 19);
            this.label8.TabIndex = 27;
            this.label8.Text = "Телефон:";
            // 
            // labelPatien
            // 
            this.labelPatien.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelPatien.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPatien.ForeColor = System.Drawing.Color.White;
            this.labelPatien.Location = new System.Drawing.Point(557, 33);
            this.labelPatien.Name = "labelPatien";
            this.labelPatien.Size = new System.Drawing.Size(87, 20);
            this.labelPatien.TabIndex = 64;
            this.labelPatien.Text = "Пацієнт";
            this.labelPatien.Click += new System.EventHandler(this.labelPatien_Click);
            // 
            // labelDoc
            // 
            this.labelDoc.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.labelDoc.Font = new System.Drawing.Font("Arial", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelDoc.ForeColor = System.Drawing.Color.White;
            this.labelDoc.Location = new System.Drawing.Point(632, 33);
            this.labelDoc.Name = "labelDoc";
            this.labelDoc.Size = new System.Drawing.Size(169, 20);
            this.labelDoc.TabIndex = 65;
            this.labelDoc.Text = "Лікар";
            this.labelDoc.Click += new System.EventHandler(this.labelDoc_Click);
            // 
            // labelAuthor
            // 
            this.labelAuthor.Font = new System.Drawing.Font("Arial", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelAuthor.ForeColor = System.Drawing.Color.White;
            this.labelAuthor.Location = new System.Drawing.Point(303, 456);
            this.labelAuthor.Name = "labelAuthor";
            this.labelAuthor.Size = new System.Drawing.Size(172, 36);
            this.labelAuthor.TabIndex = 66;
            this.labelAuthor.Text = "Copyright Yriy Trotsiuk";
            this.labelAuthor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(110)))), ((int)(((byte)(122)))));
            this.ClientSize = new System.Drawing.Size(800, 492);
            this.Controls.Add(this.labelAuthor);
            this.Controls.Add(this.panelDoctor);
            this.Controls.Add(this.labelDoc);
            this.Controls.Add(this.labelPatien);
            this.Controls.Add(this.dataGridViewPatients);
            this.Controls.Add(this.panelPatient);
            this.Controls.Add(this.labelClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "DataForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DataForm";
            this.Load += new System.EventHandler(this.DataForm_Load);
            this.panelPatient.ResumeLayout(false);
            this.panelPatient.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPatients)).EndInit();
            this.panelDoctor.ResumeLayout(false);
            this.panelDoctor.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelClose;
        private System.Windows.Forms.Label labelName;
        private System.Windows.Forms.Label labelSurname;
        private System.Windows.Forms.Label labelPatronymic;
        private System.Windows.Forms.Label labelTel;
        private System.Windows.Forms.Label labelPassport;
        private System.Windows.Forms.Label labelDoctor;
        private System.Windows.Forms.Panel panelPatient;
        private System.Windows.Forms.TextBox textBoxName;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox textBoxPassport;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBoxTel;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox textBoxPatronymic;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxSurname;
        private System.Windows.Forms.Panel panelTel;
        private System.Windows.Forms.Label labelBirthday;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox textBoxBirthday;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox textBoxDoctor;
        private System.Windows.Forms.Button buttonShowPatients;
        private System.Windows.Forms.Button buttonRemovePatient;
        private System.Windows.Forms.Button buttonAddPatient;
        private System.Windows.Forms.Label labelError;
        private System.Windows.Forms.DataGridView dataGridViewPatients;
        private System.Windows.Forms.Panel panelDoctor;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TextBox textBoxCabDoc;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox textBoxCountPatDoc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.TextBox textBoxPositionDoc;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.TextBox textBoxTelDoc;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label labelErrorDoc;
        private System.Windows.Forms.Button buttonShowDoc;
        private System.Windows.Forms.Button buttonDeleteDoc;
        private System.Windows.Forms.Button buttonAddDoc;
        private System.Windows.Forms.TextBox textBoxNameDoc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox textBoxPatronDoc;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.TextBox textBoxSurnameDoc;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label labelPatien;
        private System.Windows.Forms.Label labelDoc;
        private System.Windows.Forms.Label labelAuthor;
    }
}