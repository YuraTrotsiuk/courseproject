﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryPolyclinic
{
    public class Person
    {
        private string name;
        private string surname;
        private string patronymic;
        private string tel;

        public string Name 
        { 
            get { return name; } 
            set { name = value; }
        }
        public string Surname 
        { 
            get { return surname; } 
            set { surname = value; } 
        }

        public string Patronymic 
        { 
            get { return patronymic; } 
        }
        public string Tel
        {
            get { return tel; }
        }

        public Person(string surname,string name,string patronymic,string tel)
        {
            this.name = name;
            this.surname = surname;
            this.patronymic = patronymic;
            this.tel = tel;
        }
    }
}
