﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryPolyclinic
{
    public class User
    {
       
        private string login;
        private string password;
        
        public string Login 
        {
            get { return login; }
        }
        public string Password 
        {
            get { return password; }
        }

        public User(string l,string p)
        {
            login = l;
            password = p;
        }

    }
}
