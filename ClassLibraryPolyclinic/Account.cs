﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryPolyclinic
{
    public class Account : User
    {
        private string tel;
        public string Tel 
        {
            get { return tel; }
        }

        public Account(string t, string l, string p):base(l,p)
        {
            tel = t;
        }
    }
}
