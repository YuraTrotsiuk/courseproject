﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryPolyclinic
{
    public class Patient:Person
    {
        private string birthday;
        private string passport;
        private string doctor;

        public string Birthday
        {
            get { return birthday; }
        }
        public string Passport
        {
            get { return passport; }
        }
        public string Doctor
        {
            get { return doctor; }
        }

        public Patient(string surname,string name,string patronymic, string birthday, string tel,string passport,string doctor) : base(surname, name, patronymic, tel)
        {
            this.birthday = birthday;
            this.passport = passport;
            this.doctor = doctor;
        }
    }
}
