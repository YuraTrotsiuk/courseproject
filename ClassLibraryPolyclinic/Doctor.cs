﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryPolyclinic
{
    public class Doctor : Person
    {
        private string position;
        private string maxCountPatients;
        private string cabinet;

        public string Position
        {
            get { return position; }
        }
        public string MaxCountPatients
        {
            get { return maxCountPatients; }
        }
        public string Cabinet
        {
            get { return cabinet; }
        }

        public Doctor(string surname,string name,string patronymic,string tel,string position,string maxCountPatients,string cabinet) : base(surname, name, patronymic, tel)
        {
            this.position = position;
            this.maxCountPatients = maxCountPatients;
            this.cabinet= cabinet;
        }
    }
}
